# MorpionS6

- Gérer les modèles d'IA (suppression, affichage et gestion du fichier de configuration).  
-Dans une barre présente dans la fenêtre de jeu, on peut cliquer sur une icône de configuration qui ouvre une fenêtre permettant la configuration.
- Choix du mode de jeu (homme contre IA ou homme contre homme).  
-Dans la barre inférieure, se trouvent deux boutons, un qui lance une partie en Joueur contre Joueur, et un autre en Joueur contre IA.
- Identifier clairement le vainqueur via un traitement spécifique des pions composant la ligne du vainqueur.  
-On trace une ligne verte sur la combinaison gagnante
- Régler le niveaux de difficulté. Un utilisateur pourra changer ce niveau entre deux parties.  
-L'icône de configuration permet de sélectionner la difficulté de l'IA de la prochaine partie contre l'IA.
- Fournir, si nécessaire, un espace de visualisation de la phase d'apprentissage de l'IA. Il sera notamment ajouté un indicateur de progression de cette phase d'apprentissage dans un processus dédié (Task).  
-En bas de la fenêtre de configuration de l'IA se trouve une barre de progression.
- Proposer à l'utilisateur de modifier les paramètres du jeu via un menu dédié.  
-Dans une barre présente dans la fenêtre de jeu, on peut cliquer sur une icône de configuration qui ouvre une fenêtre permettant la configuration.
- Insérer des processus de transition vu en cours.  
-*Voir feuille de description*