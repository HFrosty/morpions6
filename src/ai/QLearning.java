package ai;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class QLearning implements Serializable {

	private static final long serialVersionUID = -394854929653400727L;
	public static final int AI = 1;
	public static final int NOTHING = 0;
	public static final int OPPONENT = -1;
	
	public static final int WIN = 0;
	public static final int DRAW = 1;
	public static final int LOSE = 2;
	public static final int NOEND = 3;
	
	private double discountFactor;
	private double learningRate;
	private double victoryReward;
	private double defeatReward;
	private double drawReward;
	private int gameCount;
	private double initialValue;
	private String name;

	private HashMap<String, double[]> q;
	
	public QLearning(String name, double discountFactor, double learningRate, double initial, double victoryReward, double defeatReward, double drawReward, int gameCount)
	{
		this.discountFactor = discountFactor;
		this.learningRate = learningRate;
		this.victoryReward = victoryReward;
		this.defeatReward = defeatReward;
		this.drawReward = drawReward;
		this.gameCount = gameCount;
		this.name = name;
		this.initialValue = initial;
		q = new HashMap<String, double[]>();
		
		// Initialize qtable
		for(int a = -1; a < 2; ++a)
		{
			for(int b = -1; b < 2; ++b)
			{
				for(int c = -1; c < 2; ++c)
				{
					for(int d = -1; d < 2; ++d)
					{
						for(int e = -1; e < 2; ++e)
						{
							for(int f = -1; f < 2; ++f)
							{
								for(int g = -1; g < 2; ++g)
								{
									for(int h = -1; h < 2; ++h)
									{
										for(int i = -1; i < 2; ++i)
										{
											int[] array = new int[] {a, b, c, d, e, f, g, h, i};
											String serialization = Tools.StateSerialization(array);
											double[] defaultValues = new double[] {initial, initial, initial, initial, initial, initial, initial, initial, initial};
											q.put(serialization, defaultValues);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	// Q-Learning reinforcement method
	public void Reinforcement(int[] state, int play, double reward)
	{
		String serialization = Tools.StateSerialization(state);
		double oldValue = q.get(serialization)[play];
		double newValue = oldValue + learningRate * (reward + discountFactor * Max(NextState(state, play)) - oldValue);
		double[] array = q.get(serialization);
		array[play] = newValue;
		q.put(serialization, array);
	}
	
	// Used to determine the maximum weight of a certain state (the weight of what is considered the best play of the given state)
	public double Max(int[] state)
	{
		double[] array = q.get(Tools.StateSerialization(state));
		Double max = null;
		for(int i = 0; i < 9; ++i)
		{
			if(max == null || array[i] > max)
			{
				max = array[i];
			}
		}
		return max;
	}
	
	// Used to get the plays that are the most interesting (those whom share the highest weight)
	public ArrayList<Integer> PlausiblePlays(int[] state)
	{
		ArrayList<Integer> plausiblePlays = new ArrayList<Integer>();
		double max = 0.0;
		String serialization = Tools.StateSerialization(state);
		double[] array = q.get(serialization);
		for(int i = 0; i < 9; ++i)
		{
			if(state[i] == NOTHING)
			{
				if(plausiblePlays.size() == 0)
				{
					max = array[i];
					plausiblePlays.add(i);
				}
				else if(array[i] > max)
				{
					max = array[i];
					plausiblePlays.removeAll(plausiblePlays);
					plausiblePlays.add(i);
				}
				else if(array[i] == max)
				{
					plausiblePlays.add(i);
				}
			}
		}
		
		// Randomly choose one play
		return plausiblePlays;
	}
	
	// Gives a random play from the legal ones of the given state
	public int SelectRandomPlay(int[] state)
	{
		ArrayList<Integer> legalPlays = new ArrayList<Integer>();
		for(int i = 0; i < 9; ++i)
		{
			if(state[i] == NOTHING)
			{
				legalPlays.add(i);
			}
		}
		return legalPlays.get(new Random().nextInt(legalPlays.size()));
	}
	
	/*
	 * Method used to train the AI
	 * What happens is that the AI plays a game against an opponent that plays randomly, and determines 
	 */
	public void Train()
	{
		// Generate clean board
		int[] board = new int[] {NOTHING, NOTHING, NOTHING, NOTHING, NOTHING, NOTHING, NOTHING, NOTHING, NOTHING};
		
		// We create a list to keep track of the AI plays
		ArrayList<Integer> moves = new ArrayList<Integer>();
		ArrayList<int[]> states = new ArrayList<int[]>();
		
		int end = NOEND;
		while(end == NOEND)
		{
			// Dummy opponent plays randomly on one of the legal moves
			int opponentPlay = SelectRandomPlay(board);
			board[opponentPlay] = OPPONENT;
			
			end = CheckEnd(board);
			
			int[] newState = board.clone();
			states.add(newState); // We store the state to get a couple (State, Move)
			if(end == NOEND)
			{
				// AI randomly chooses its move from the ones having the highest value for the current state
				ArrayList<Integer> plausiblePlays = PlausiblePlays(board);
				int aiPlay = plausiblePlays.get(new Random().nextInt(plausiblePlays.size()));
				board[aiPlay] = AI;
				moves.add(aiPlay); // We store the move to reinforce the couple (State, Move) according to the result of the game (Victory, defeat, draw)
			}
			
			end = CheckEnd(board);
		}
		
		// According to how the game has ended, the define the appropriate reward
		double reward = defeatReward;
		if(end == DRAW)
		{
			reward = drawReward;
		}
		else if(end == WIN)
		{
			reward = victoryReward;
		}
		
		// For each move, we reinforce them in the state in which they occurred
		for(int j = 0; j < moves.size(); ++j)
		{
			Reinforcement(states.get(j), moves.get(j), reward);
		}	
	}
	
	// Used to calculate what will be the state if we play a certain move on the current state
	public int[] NextState(int[] currentState, int play)
	{
		int[] clone = currentState.clone();
		clone[play] = 1;
		return clone;
	}
	
	public int CheckEnd(int[] state)
	{
		// Check lines
		for(int i = 0; i < 3; ++i)
		{
			int position = i*3;
			if(state[position] != NOTHING && state[position] == state[position+1] && state[position] == state[position+2])
			{
				if(state[position] == AI)
				{
					return WIN;
				}
				else
				{
					return LOSE;
				}
			}
		}
		
		// Check columns
		for(int i = 0; i < 3; ++i)
		{
			int position = i;
			if(state[i] != NOTHING && state[i] == state[i+3] && state[i] == state[i+6])
			{
				if(state[position] == AI)
				{
					return WIN;
				}
				else
				{
					return LOSE;
				}
			}
		}
		
		// Check first diagonal
		if(state[0] != NOTHING && state[0] == state[4] && state[0] == state[8])
		{
			if(state[0] == AI)
			{
				return WIN;
			}
			else
			{
				return LOSE;
			}
		}
		
		// Check second diagonal
		if(state[2] != NOTHING && state[2] == state[4] && state[2] == state[6])
		{
			if(state[2] == AI)
			{
				return WIN;
			}
			else
			{
				return LOSE;
			}
		}
		
		// Check if board is full
		for(int i = 0; i < 9; ++i)
		{
			if(state[i] == NOTHING)
			{
				return NOEND;
			}
		}
		return DRAW;
	}
	
	public String GetName()
	{
		return name;
	}
	
	public double GetDiscountFactor()
	{
		return discountFactor;
	}

	public double GetLearningRate()
	{
		return learningRate;
	}

	public double GetVictoryReward()
	{
		return victoryReward;
	}

	public double GetDefeatReward()
	{
		return defeatReward;
	}

	public double GetDrawReward()
	{
		return drawReward;
	}
	
	public int GetGameCount()
	{
		return gameCount;
	}

	public double GetInitialValue()
	{
		return initialValue;
	}
	
}
