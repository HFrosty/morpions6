package ai;

public abstract class Tools {

	// We serialize a state, to store in a HashMap of string
	public static String StateSerialization(int[] state)
	{
		String result = "";
		for(int i = 0; i < 9; ++i)
		{
			result += Integer.toString(state[i]);
		}
		return result;
	}
	
}
