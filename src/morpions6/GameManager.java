package morpions6;

import java.io.File;

public class GameManager {

	private boolean paused;
	private Gamemode gamemode;
	private Grid grid;
	static private GameManager singleton;
	private AITrainerThread aiTrainer;
	private AIMorp currentAI;
	private AIMorp nextAI;
	
	// At creation, the user is in Singleplayer
	private GameManager()
	{
		paused = false;
		grid = new Grid();
		gamemode = new Singleplayer();
		aiTrainer = null;
		nextAI = null;
		currentAI = null;
	}
	
	static public void Initialize()
	{
		if(singleton != null)
		{
			return;
		}
		singleton = new GameManager();
		// Load last used AI
		File currentAIFile = AIFileManager.FetchCurrentAi();
		singleton.currentAI = new AIMorp(AIFileManager.LoadAI(currentAIFile));
		
		singleton.grid.Reset();
		FXGamemodeIndicator.Update();
		FXMainWindow.GetSingleplayerButton().setDisable(true);
		FXMainWindow.GetMultiplayerButton().setDisable(false);
	}
	
	// Used to check if the game is paused
	static public boolean Playable()
	{
		return !singleton.paused;
	}
	
	// Used to pause the game
	static public void Pause()
	{
		singleton.paused = true;
	}
	
	// Used to unpause the game
	static public void Unpause()
	{
		singleton.paused = false;
	}
	
	// When changing the gamemode, we reset the grid, update the display at the top and render unusable the new gamemode button
	static public void SetGamemode(Gamemode mode)
	{
		singleton.gamemode = mode;
		singleton.grid.Reset();
		FXGamemodeIndicator.Update();
		
		if(mode.Type() == GamemodeType.SINGLEPLAYER)
		{
			FXMainWindow.GetSingleplayerButton().setDisable(true);
			FXMainWindow.GetMultiplayerButton().setDisable(false);
		}
		else
		{
			FXMainWindow.GetSingleplayerButton().setDisable(false);
			FXMainWindow.GetMultiplayerButton().setDisable(true);
		}
	}
	
	// Used to set the AI that will be used next game
	static public void SetNextAI(AIMorp ai)
	{
		singleton.nextAI = ai;
		AIFileManager.SetCurrentAI(ai.GetQLearning());
	}
	
	// Used to train a new AI and display info to the user
	static public void ConfigureAndLaunchAITraining(AITraining training)
	{
		if(IsInTraining()) // If a training is operating, we stop and inform the user -----> Obsolete, as the buttons are greyed
		{
			FXAIWindow.promptMessage += "Impossible de d�marrer la cr�ation d'une Intelligence Artificielle, une est d�j� en cours.\n";
			FXAIWindow.UpdatePromptMessage();
			System.out.println("ERROR: AI Trainer is already on");
			return;
		}
		FXAIWindow.promptMessage = "Cr�ation d'une Intelligence Artificielle en cours...\n";
		if(training.GetToUseNextGame()) // We inform the user if the AI is set to be used next game
		{
			FXAIWindow.promptMessage += "L'IA sera utilis�e � la partie suivant sa cr�ation.\n";
		}
		else
		{
			FXAIWindow.promptMessage += "L'IA ne sera pas utilis�e automatiquement.\n";
		}
		FXAIWindow.UpdatePromptMessage();
		singleton.aiTrainer = new AITrainerThread(training);
		singleton.aiTrainer.start();
	}
	
	static public void GoToNextAIIfOne()
	{
		if(singleton.nextAI != null) // If an AI is waiting to be used, we use it
		{
			singleton.currentAI = singleton.nextAI;
			singleton.nextAI = null;
		}
	}
	
	// We kill the current trainer
	static public void OnTrainingFinished()
	{
		singleton.aiTrainer = null;
	}

	// We stop the thread and kill the trainer (Sleep of 1 ms is to fix an issue with the progress bar)
	public static void CancelTraining()
	{
		singleton.aiTrainer.stop();
		singleton.aiTrainer = null;
		try
		{
			Thread.sleep(1);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		FXAIWindow.FillProgressBar(0.0);
	}
	
	// If a trainer exist, it means that a training is operating
	public static boolean IsInTraining()
	{
		return singleton.aiTrainer != null;
	}
	
	static public Gamemode GetGamemode()
	{
		return singleton.gamemode;
	}
	
	static public Grid GetGrid()
	{
		return singleton.grid;
	}
	
	static public AIMorp GetCurrentAI()
	{
		return singleton.currentAI;
	}
}
