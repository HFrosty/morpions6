package morpions6;

import java.io.File;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class ImageFactory {
	
	static private final String RESOURCES = "./resources/";
	
	// Used across the program to build the correct image for each symbol
	static public Image BuildImageForSymbol(Symbol symbol)
	{
		File file;
		if(symbol.Type() == SymbolType.CIRCLE)
		{
			file = new File(RESOURCES + "circle.png");
		}
		else
		{
			file = new File(RESOURCES + "cross.png");
		}
		return new Image(file.toURI().toString());
	}
	
	// Load image and create ImageView according to the button name, used image files should be of size 80x80 maximum
	public static ImageView CreateButtonImageView(String buttonName)
	{
		File file;
		if(buttonName == "RESTART")
		{
			file = new File(RESOURCES + "restart.png");
		}
		else if(buttonName == "SINGLEPLAYER")
		{
			file = new File(RESOURCES + "singleplayer.png");
		}
		else if(buttonName == "MULTIPLAYER")
		{
			file = new File(RESOURCES + "multiplayer.png");
		}
		else if(buttonName == "AI")
		{
			file = new File(RESOURCES + "ai.png");
		}
		else if(buttonName == "HELP")
		{
			file = new File(RESOURCES + "help.png");
		}
		else
		{
			System.out.println("ERROR: Unvalid button name as arguement '" + buttonName + "'");
			file = new File("");
		}
		Image image = new Image(file.toURI().toString());
		ImageView view = new ImageView(image);
		return view;
	}
	
	public static Image CreateGridImage()
	{
		File file = new File(RESOURCES + "grid.png");
		return new Image(file.toURI().toString());
	}
	
	// Depending on the type of the current gamemode, we create the according resource
	public static Image CreateGamemodeImage(Gamemode mode)
	{
		File file;
		if(mode.Type() == GamemodeType.SINGLEPLAYER)
		{
			file = new File(RESOURCES + "singleplayer.png");
		}
		else
		{
			file = new File(RESOURCES + "multiplayer.png");
		}
		return new Image(file.toURI().toString());
	}
	
	// Depending on the victory type, (horizontal, vertical...) we return the correct image
	public static Image CreateImageForHelp(String victoryType)
	{
		File file;
		if(victoryType.equals("horizontal"))
		{
			file = new File(RESOURCES + "help1.png");
		}
		else if(victoryType.equals("vertical"))
		{
			file = new File(RESOURCES + "help2.png");
		}
		else
		{
			file = new File(RESOURCES + "help3.png");
		}
		return new Image(file.toURI().toString());
	}

}
