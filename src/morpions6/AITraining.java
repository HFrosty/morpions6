package morpions6;

import ai.QLearning;

// Class used to train an AI in a separate thread
public class AITraining implements Runnable {

	private QLearning apprentice;
	private boolean toUseNextGame;
	private static final int FIRSTBATCHSIZE = 10000;
	
	public AITraining(QLearning apprentice, boolean toUse)
	{
		this.apprentice = apprentice;
		toUseNextGame = toUse;
	}

	// Method that trains the AI
	@Override
	public void run()
	{		
		int gameCount = apprentice.GetGameCount();
		FXAIWindow.promptMessage += "Temps total estim� : ";
		if(gameCount < FIRSTBATCHSIZE) // If the amount of games to do to train the AI is low, we estimate that it will take less than 0.1s to train the AI
		{
			FXAIWindow.promptMessage += " < 0.1s\n";
			FXAIWindow.UpdatePromptMessage();
			for(int i = 0; i < gameCount; ++i)
			{
				TrainingRoutine(i, gameCount);
			}
		}
		else // If not, we measure the time of the first few trainings and we use it to estimate the time of all the trainings, then we do the trainings left
		{
			long startTime = System.nanoTime();
			for(int i = 0; i < FIRSTBATCHSIZE; ++i)
			{
				TrainingRoutine(i, gameCount);
			}
			long totalTime = (System.nanoTime() - startTime);
			long estimatedTime = totalTime * (gameCount / FIRSTBATCHSIZE) / 1000000000;
			FXAIWindow.promptMessage += " " + estimatedTime + "s\n";
			FXAIWindow.UpdatePromptMessage();
			
			for(int i = FIRSTBATCHSIZE; i < gameCount; ++i)
			{
				TrainingRoutine(i, gameCount);
			}
		}
		
		// We notifiy the AI window that it is now possible to modify the fields and we set the progress bar to 100%
		FXAIWindow.SetModificationsPossible(true);
		FXAIWindow.FillProgressBar(1.0);
		if(toUseNextGame) // If the user specified that he want to AI to be used the game following its creation, we inform the GameManager to do so
		{
			GameManager.SetNextAI(new AIMorp(apprentice));	
		}
		GameManager.OnTrainingFinished(); // We tell the Game Manager that the training is finished
		
		// Save ai file
		AIFileManager.SaveAI(apprentice);
	}
	
	// We train the AI and we fill the progress bar accordingly
	public void TrainingRoutine(int index, int gameCount)
	{
		apprentice.Train();
		FXAIWindow.FillProgressBar((double)index / (double)gameCount);
	}
	
	public boolean GetToUseNextGame()
	{
		return toUseNextGame;
	}
	
}
