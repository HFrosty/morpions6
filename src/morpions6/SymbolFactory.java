package morpions6;

public abstract class SymbolFactory {
	
	// We create a symbol for each player
	static public Symbol CreateSymbolAccordingToPlayer(PlayerID player)
	{
		if(player == PlayerID.Player1)
		{
			return new Cross();
		}
		else
		{
			return new Circle();
		}
	}
}
