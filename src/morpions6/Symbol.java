package morpions6;

// Interface used to tell the type of a symbol
public interface Symbol {
	public SymbolType Type();
}
