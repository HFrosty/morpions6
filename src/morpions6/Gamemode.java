package morpions6;

// Interface used to tell if a gamemode is multiplayer or singleplayer
public interface Gamemode {
	public GamemodeType Type();
}
