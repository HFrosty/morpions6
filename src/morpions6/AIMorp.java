package morpions6;

import java.util.ArrayList;
import java.util.Random;

import ai.QLearning;

// This class is used by the Game Manager to store a QLearning that is asked to tell us what it would play
public class AIMorp {
	
	private QLearning qlearning;
	
	public AIMorp(QLearning qlearning)
	{
		this.qlearning = qlearning;
	}
	
	public void PlayPosition()
	{
		// Convert current board to state and build legal plays
		int[] state = new int[9];
		Grid grid = GameManager.GetGrid();
		ArrayList<Symbol> board = grid.GetBoard();
		for(int i = 0; i < board.size(); ++i)
		{
			if(board.get(i) == null)
			{
				state[i] = QLearning.NOTHING;
			}
			else
			{
				if(board.get(i).Type() == SymbolType.CIRCLE)
				{
					state[i] = QLearning.AI;
				}
				else
				{
					state[i] = QLearning.OPPONENT;
				}
			}
		}
		
		// Select randomly from plausible plays
		ArrayList<Integer> plausiblePlays = qlearning.PlausiblePlays(state);
		grid.AddSymbol(plausiblePlays.get(new Random().nextInt(plausiblePlays.size())));
	}
	
	public String GetName()
	{
		return qlearning.GetName();
	}

	public QLearning GetQLearning()
	{
		return qlearning;
	}
	
}
