package morpions6;

import java.util.ArrayList;

// Grid class, that has info on the current state of the game, and update the display
public class Grid {

	private ArrayList<Symbol> board;
	private FXGrid fxgrid;
	private PlayerID currentPlayer;
	private boolean ended;
	
	public Grid() {
		board = null;
		fxgrid = new FXGrid(this);
		currentPlayer = null;
		ended = false;
	}
	
	public void AddSymbol(int position)
	{
		// We create a symbol according to the current player, add it on the board and draw it on the grid
		Symbol symbol = SymbolFactory.CreateSymbolAccordingToPlayer(currentPlayer);
		board.set(position, symbol);
		fxgrid.DrawSymbol(symbol, position);
		
		// After adding the symbol, we check if the game has been won
		CheckVictory();
		
		// If the game has not ended, we swap players and player indicator at the bottom left of the screen
		if(!ended)
		{
			SwapPlayers();
			FXPlayerIndicator.Update();
		}
	}

	// What happens when we click on the grid
	public void OnClick(int position)
	{
		// If the game has ended, do nothing
		if(ended)
		{
			System.out.println("ERROR: Unvalid click, game has already ended");
			return;
		}
		
		if(position >= 9) // Should not happen
		{
			System.out.println("ERROR: Unvalid click position " + position);
			return;
		}
		
		// Check if clicked position is valid
		if(board.get(position) != null)
		{
			System.out.println("ERROR: Unvalid click position " + position + ", there is already a symbol of type " + board.get(position).Type());
			return;
		}
		
		// Do nothing if the game is paused
		if(!GameManager.Playable())
		{
			return;
		}
		
		this.AddSymbol(position);
		
		// If we are in singleplayer and the game hasn't ended, make the AI play
		if(GameManager.GetGamemode().Type() == GamemodeType.SINGLEPLAYER && !ended)
		{
			GameManager.GetCurrentAI().PlayPosition();
		}
	}
	
	public void CheckVictory()
	{
		// Check lines
		for(int i = 0; i < 3 && !ended; ++i)
		{
			int crossCount = 0;
			int circleCount = 0;
			for(int j = 0; j < 3; ++j)
			{
				if(board.get(i*3 + j) != null)
				{
					if(board.get(i*3+j).Type() == SymbolType.CIRCLE)
					{
						circleCount++;
					}
					else
					{
						crossCount++;
					}
				}
			}
			if(circleCount == 3 || crossCount == 3)
			{
				ended = true;
				fxgrid.LaunchVictoryAnimation(i*3, i*3 + 1, i*3+2);
			}
		}
		
		// Check columns
		for(int i = 0; i < 3 && !ended; ++i)
		{
			int crossCount = 0;
			int circleCount = 0;
			for(int j = 0; j < 3; ++j)
			{
				if(board.get(i + j*3) != null)
				{
					if(board.get(i+j*3).Type() == SymbolType.CIRCLE)
					{
						circleCount++;
					}
					else
					{
						crossCount++;
					}
				}
			}
			if(circleCount == 3 || crossCount == 3)
			{
				ended = true;
				fxgrid.LaunchVictoryAnimation(i, i + 3, i+6);
			}
		}
		
		// Check first diagonal
		if(!ended && board.get(0) != null && board.get(4) != null && board.get(8) != null)
		{
			SymbolType firstType = board.get(0).Type();
			if(firstType == board.get(4).Type() && firstType == board.get(8).Type())
			{
				ended = true;
				fxgrid.LaunchVictoryAnimation(0, 4, 8);
			}
		}
		
		// Check second diagonal
		if(!ended && board.get(2) != null && board.get(4) != null && board.get(6) != null)
		{
			SymbolType firstType = board.get(2).Type();
			if(firstType == board.get(4).Type() && firstType == board.get(6).Type())
			{
				ended = true;
				fxgrid.LaunchVictoryAnimation(2, 4, 6);
			}
		}
		
		// Check if draw match
		for(int i = 0; i < board.size(); ++i)
		{
			if(board.get(i) == null)
			{
				return;
			}
		}
		ended = true;
	}
	
	// We reset the grid at the initial stage
	public void Reset()
	{
		ended = false;
		board = new ArrayList<Symbol>();
		for(int i = 0; i < 9; ++i) {
			board.add(null);
		}
		currentPlayer = PlayerID.Player1;
		FXPlayerIndicator.Update();
		GameManager.GoToNextAIIfOne();
		fxgrid.Reset();
		FXMainWindow.UpdateAIName();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<Symbol> GetBoard()
	{
		return (ArrayList<Symbol>) board.clone();
	}
	
	public void SwapPlayers()
	{
		if(currentPlayer == PlayerID.Player1)
		{
			currentPlayer = PlayerID.Player2;
		}
		else
		{
			currentPlayer = PlayerID.Player1;
		}
	}
	
	public PlayerID GetCurrentPlayer()
	{
		return currentPlayer;
	}
	
}
