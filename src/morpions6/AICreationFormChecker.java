package morpions6;

import javafx.scene.control.TextField;

public abstract class AICreationFormChecker {
	
	private static final char[] FILEILLEGALCHAR = { '/', '\n', '\r', '\t', '\0', '\f', '`', '?', '*', '\\', '<', '>', '|', '\"', ':' };

	// Function used to throw an exception if the fields used for the creation of an AI are invalid. The exception's message gives intructions to the user about what's wrong
	public static void CheckFields(TextField nameField, TextField discountField, TextField learningField, TextField gameField, TextField initField, TextField victoryField, TextField drawField, TextField defeatField) throws Exception
	{
		String message = "";
		
		Double learningRate = null;
		Double discountFactor = null;
		Integer gameCount = null;
		
		for(int i = 0; i < FILEILLEGALCHAR.length; ++i)
		{
			if(nameField.getText().contains("" + FILEILLEGALCHAR[i]))
			{
				message += "Caract�re " + FILEILLEGALCHAR[i] + " interdit dans le nom de l'IA.\n";
			}
		}
		
		try
		{
			learningRate = Double.parseDouble(learningField.getText());
		}
		catch (NumberFormatException e)
		{
			message += "Learning Rate doit correspondre � un nombre � virgule." + "\n";
		}
		
		try
		{
			discountFactor = Double.parseDouble(discountField.getText());
		}
		catch (NumberFormatException e)
		{
			message += "Discount Factor doit correspondre � un nombre � virgule." + "\n";
		}
		
		try
		{
			Double.parseDouble(initField.getText());
		}
		catch (NumberFormatException e)
		{
			message += "Valeur initiale doit correspondre � un nombre � virgule." + "\n";
		}
		
		try
		{
			Double.parseDouble(victoryField.getText());
		}
		catch (NumberFormatException e)
		{
			message += "R�compense - Victoire doit correspondre � un nombre � virgule." + "\n";
		}
		
		try
		{
			Double.parseDouble(drawField.getText());
		}
		catch (NumberFormatException e)
		{
			message += "R�compense - Nul doit correspondre � un nombre � virgule." + "\n";
		}
		
		try
		{
			Double.parseDouble(defeatField.getText());
		}
		catch (NumberFormatException e)
		{
			message += "R�compense - D�faite doit correspondre � un nombre � virgule." + "\n";
		}
		
		try
		{
			gameCount = Integer.parseInt(gameField.getText());
		}
		catch (NumberFormatException e)
		{
			message += "Nombre de parties doit correspondre � un nombre entier." + "\n";
		}
		
		if(discountFactor != null && (discountFactor <= 0 || discountFactor >= 1))
		{
			message += "Discount Factor doit satisfaire : 0 < Discount Factor < 1.\n";
		}
		
		if(learningRate != null && (learningRate <= 0 || learningRate > 1))
		{
			message += "Learning Rate doit satisfaire : 0 < Learning Rate <= 1.\n";
		}
		
		if(gameCount != null && gameCount <= 0)
		{
			message += "Nombre de parties doit �tre sup�rieur � 0.\n";
		}
		
		if(!message.equals(""))
		{
			throw new Exception(message);
		}
		
	}
	
}
