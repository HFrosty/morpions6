package morpions6;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

// Class SubWindow, parent to AIWindow and HelpWindow
public abstract class FXSubWindow {

	protected Stage stage;

	// We create a window using specified FXML file. Most importantly we pause the game at creation and set that the subwindow must always be on top
	public FXSubWindow(String fxml, String name, int sizeX, int sizeY) throws Exception
	{
		GameManager.Pause();
		
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(fxml));
		loader.setController(this);
		Parent content = loader.load();
		stage = new Stage();
		stage.setTitle(name);
		stage.setScene(new Scene(content, sizeX, sizeY));
		stage.show();
		
		stage.setOnHiding((event) -> 
		{
			OnClose();
		});
		
		stage.setAlwaysOnTop(true);
	}
	
	public void OnClose()
	{
		GameManager.Unpause();
	}
}
