package morpions6;

// This class is used at the creation of the main window to tell what is supposed to happen when the user click a button located at the bottom of the window
public abstract class BottomButtonsBehaviour {

	public static void StartSingleplayer()
	{
		if(GameManager.Playable())
		{
			GameManager.SetGamemode(new Singleplayer());
		}
	}
	
	public static void StartMultiplayer()
	{
		if(GameManager.Playable())
		{
			GameManager.SetGamemode(new Multiplayer());
		}
	}
	
	public static void OpenHelp()
	{
		if(GameManager.Playable())
		{
			try {
				new FXHelpWindow();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void Restart()
	{
		if(GameManager.Playable())
		{
			GameManager.GetGrid().Reset();
		}
	}
	
	public static void OpenAISetup()
	{
		if(GameManager.Playable())
		{
			try {
				new FXAIWindow();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
}
