package morpions6;

import java.io.File;

import ai.QLearning;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

public class FXAIWindow extends FXSubWindow {

	private static FXAIWindow instance;
	public static String promptMessage = "";
	
	public FXAIWindow() throws Exception
	{
		super("ai.fxml", "Configuration IA", 640, 480); // We create the window
		SetBehaviours();
		instance = this; // We store as a static pointer the new window, so it can be used by the AITraining class to update the display
		UpdatePromptMessage();
		cancelButton.setDisable(true);
		
		// We check if an AI is in training, and if so, we disable the form
		if(GameManager.IsInTraining())
		{
			FXAIWindow.SetModificationsPossible(false);
		}
		
		DisplayInForm(GameManager.GetCurrentAI().GetQLearning()); // We display in the form the AI that is currently playing
	}
	
	// We inform the program that on close, the instance of FXAIWindow becomes null, this instance is used by following methods
	@Override
	public void OnClose()
	{
		super.OnClose();
		instance = null;
	}
	
	// We set what happens when the user click on the create button
	private void OnCreateButtonClick()
	{
		// Check if user input is valid, if not, we stop here and inform him of what is wrong
		try
		{
			AICreationFormChecker.CheckFields(nameField, discountField, learningField, gameField, initField, victoryField, drawField, defeatField);
		}
		catch (Exception e)
		{
			promptMessage = e.getMessage();
			UpdatePromptMessage();
			return;
		}
		
		String name = nameField.getText();
		double discount = Double.parseDouble(discountField.getText());
		double learning = Double.parseDouble(learningField.getText());
		int gameCount = Integer.parseInt(gameField.getText());
		double init = Double.parseDouble(initField.getText());
		double victory = Double.parseDouble(victoryField.getText());
		double draw = Double.parseDouble(drawField.getText());
		double defeat = Double.parseDouble(defeatField.getText());
		boolean toUse = useAfterCreation.isSelected();
		
		// Creation of a training and its apprentice
		AITraining training = new AITraining(new QLearning(name, discount, learning, init, victory, defeat, draw, gameCount), toUse); 
		
		// We start the training of an AI and restrict the user from modifying the fields
		GameManager.ConfigureAndLaunchAITraining(training);
		SetModificationsPossible(false);
	}
	
	// We set what happens when the user click on the load button
	private void OnLoadButtonClick()
	{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Ouvrir configuration IA");
		fileChooser.setInitialDirectory(new File(AIFileManager.FOLDER));
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Config IA", "*" + AIFileManager.SUFFIX), new FileChooser.ExtensionFilter("Tous les fichiers", "*"));
		File file = fileChooser.showOpenDialog(stage);
		
		// If the user has selected no file, we do nothing
		if(file == null)
		{
			return;
		}
		
		// We load the selected AI, display it in the form and set it as the AI that will be played next round
		QLearning learning = AIFileManager.LoadAI(file);
		DisplayInForm(learning);
		GameManager.SetNextAI(new AIMorp(learning));
		
		// We inform the user of what happened
		promptMessage = "Chargement de l'IA " + learning.GetName() + " r�ussie. Celle-ci sera utilis�e � la prochaine partie.";
		UpdatePromptMessage();
	}
	
	// When the user click on the cancel button, we cancel the current training and allow him to modify the form
	private void OnCancelButtonClick()
	{
		SetModificationsPossible(true);
		GameManager.CancelTraining();
		
		promptMessage = "Cr�ation de l'IA annul�e.";
		UpdatePromptMessage();
	}
	
	// We set what happens if the user click on the delete button
	private void OnDeleteButtonClick()
	{
		String filename = nameField.getText() + AIFileManager.SUFFIX;
		File file = AIFileManager.FetchAiFile(filename);
		// If the file of given name exists, we delete it, if not, we inform the user of the situation
		if(file.exists())
		{
			file.delete();
			promptMessage = "Suppression du fichier " + filename + " r�ussie.\nNotez toutefois que si l'IA supprim�e est utilis�e ou que son utlisation est pr�vue � la prochaine partie, celle-ci sera utilis�e.";
		}
		else
		{
			promptMessage = "Suppression du fichier " + filename + " impossible. Celui-ci n'existe pas.";
		}
		UpdatePromptMessage();
	}
	
	// We display the information of a QLearning strucure in the form
	private void DisplayInForm(QLearning learning)
	{
		nameField.setText(learning.GetName());
		discountField.setText(Double.toString(learning.GetDiscountFactor()));
		learningField.setText(Double.toString(learning.GetLearningRate()));
		gameField.setText(Integer.toString(learning.GetGameCount()));
		initField.setText(Double.toString(learning.GetInitialValue()));
		victoryField.setText(Double.toString(learning.GetVictoryReward()));
		drawField.setText(Double.toString(learning.GetDrawReward()));
		defeatField.setText(Double.toString(learning.GetDefeatReward()));
	}
	
	// We set what is supposed to happen when we restrict/allow the user to modify the fields
	public static void SetModificationsPossible(boolean bool)
	{
		if(instance != null) // Only if an FXAIWindow is opened
		{
			if(bool)
			{
				instance.cancelButton.setDisable(true);
				instance.discountField.setDisable(false);
				instance.learningField.setDisable(false);
				instance.gameField.setDisable(false);
				instance.initField.setDisable(false);
				instance.victoryField.setDisable(false);
				instance.drawField.setDisable(false);
				instance.defeatField.setDisable(false);
				instance.createButton.setDisable(false);
				instance.loadButton.setDisable(false);
				instance.useAfterCreation.setDisable(false);
				instance.deleteButton.setDisable(false);
			}
			else
			{
				instance.cancelButton.setDisable(false);
				instance.discountField.setDisable(true);
				instance.learningField.setDisable(true);
				instance.gameField.setDisable(true);
				instance.initField.setDisable(true);
				instance.victoryField.setDisable(true);
				instance.drawField.setDisable(true);
				instance.defeatField.setDisable(true);
				instance.createButton.setDisable(true);
				instance.loadButton.setDisable(true);
				instance.useAfterCreation.setDisable(true);
				instance.deleteButton.setDisable(true);
			}	
		}
	}
	
	private void SetBehaviours()
	{
		this.createButton.setOnMouseClicked((event) ->
		{
			OnCreateButtonClick();
		});
		
		this.loadButton.setOnMouseClicked((event) -> 
		{
			OnLoadButtonClick();
		});
		
		this.cancelButton.setOnMouseClicked((event) -> 
		{
			OnCancelButtonClick();
		});
		
		this.deleteButton.setOnMouseClicked((event) ->
		{
			OnDeleteButtonClick();
		});
	}
	
	public static void UpdatePromptMessage()
	{
		if(instance != null) // Only if an FXAIWindow is opened
		{
			instance.prompt.setText(promptMessage);
		}
	}
	
	public static void FillProgressBar(double value)
	{
		if(instance != null) // Only if an FXAIWindow is opened
		{
			instance.progress.setProgress(value);
		}
	}
	
	public static FXAIWindow GetInstance()
	{
		return instance;
	}
	
	public ProgressBar GetProgressBar()
	{
		return progress;
	}
	
	@FXML
	private TextField nameField;

	@FXML
	private TextField discountField;
	
	@FXML
	private TextField learningField;
	
	@FXML
	private TextField gameField;
	
	@FXML
	private TextField initField;
	
	@FXML
	private TextField victoryField;
	
	@FXML
	private TextField drawField;
	
	@FXML
	private TextField defeatField;
	
	@FXML
	private TextArea prompt;
	
	@FXML
	private Button createButton;
	
	@FXML
	private Button loadButton;
	
	@FXML
	private Button deleteButton;
	
	@FXML
	private ProgressBar progress;
	
	@FXML
	private Button cancelButton;
	
	@FXML
	private CheckBox useAfterCreation;
}
