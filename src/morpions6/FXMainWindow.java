package morpions6;

import java.io.File;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class FXMainWindow extends Application {
	
	private static FXMainWindow singleton;
	
	public FXMainWindow()
	{
	}
	
	static public void Create()
	{
		if(singleton != null)
		{
			return;
		}
		singleton = new FXMainWindow();
		FXMainWindow.launch();
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("main.fxml"));
		loader.setController(singleton);
		Parent content = loader.load();
		primaryStage.setTitle("Morpion");
		primaryStage.setScene(new Scene(content, 600, 600));
		primaryStage.setResizable(false);
		primaryStage.show();
		SetBehaviours();
		SetButtonsImages();
		Startup.StartupBehaviour();
	}

	// We set what happens when we click on the bottom buttons
	static private void SetBehaviours()
	{
		singleton.oneplayerButton.setOnMouseClicked((event) -> {
			BottomButtonsBehaviour.StartSingleplayer();
		});
		singleton.twoplayersButton.setOnMouseClicked((event) -> {
			BottomButtonsBehaviour.StartMultiplayer();
		});
		singleton.restartButton.setOnMouseClicked((event) -> {
			BottomButtonsBehaviour.Restart();
		});
		singleton.helpButton.setOnMouseClicked((event) -> {
			BottomButtonsBehaviour.OpenHelp();
		});
		singleton.aiButton.setOnMouseClicked((event) -> {
			BottomButtonsBehaviour.OpenAISetup();;
		});
	}
	
	// Used to set the images of the buttons
	static private void SetButtonsImages()
	{
		singleton.restartButton.setGraphic(ImageFactory.CreateButtonImageView("RESTART"));
		singleton.oneplayerButton.setGraphic(ImageFactory.CreateButtonImageView("SINGLEPLAYER"));
		singleton.twoplayersButton.setGraphic(ImageFactory.CreateButtonImageView("MULTIPLAYER"));
		singleton.helpButton.setGraphic(ImageFactory.CreateButtonImageView("HELP"));
		singleton.aiButton.setGraphic(ImageFactory.CreateButtonImageView("AI"));
	}
	
	public static void UpdateAIName()
	{
		// Hide it if in multiplayer, display the correct one if in singleplayer
		if(GameManager.GetGamemode().Type() == GamemodeType.MULTIPLAYER)
		{
			singleton.aiName.setText("");	
		}
		else
		{
			// Display current AI name
			singleton.aiName.setText(GameManager.GetCurrentAI().GetName());	
		}
	}

	static public AnchorPane GetGridPane()
	{
		return singleton.gridPane;
	}
	
	static public ImageView GetPlayerIndicatorImageView()
	{
		return singleton.playerIndicator;
	}
	
	static public ImageView GetGamemodeIndicatorImageView()
	{
		return singleton.gamemodeIndicator;
	}
	
	public static ImageView GetGridImageView()
	{
		return singleton.gridImageView;
	}
	
	public static Button GetSingleplayerButton()
	{
		return singleton.oneplayerButton;
	}
	
	public static Button GetMultiplayerButton()
	{
		return singleton.twoplayersButton;
	}
	
	// FXML elements put at the bottom to avoid polluting code
	@FXML
	private AnchorPane gridPane;
	
	@FXML
	private ImageView playerIndicator;
	
	@FXML
	private ImageView gamemodeIndicator;
	
	@FXML
	private Button aiButton;
	
	@FXML
	private Button helpButton;
	
	@FXML
	private Button oneplayerButton;
	
	@FXML
	private Button twoplayersButton;
	
	@FXML
	private Button restartButton;
	
	@FXML
	private ImageView gridImageView;
	
	@FXML
	private Text aiName;
}
