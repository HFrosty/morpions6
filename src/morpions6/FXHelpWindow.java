package morpions6;

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;

public class FXHelpWindow extends FXSubWindow {
	
	public FXHelpWindow() throws Exception {
		super("help.fxml", "Aide", 600, 600); // We create the window

		// We set the images
		verticalVictory.setImage(ImageFactory.CreateImageForHelp("vertical"));
		horizontalVictory.setImage(ImageFactory.CreateImageForHelp("horizontal"));
		diagonalVictory.setImage(ImageFactory.CreateImageForHelp("diagonal"));
	}

	@FXML
	private ImageView verticalVictory;
	
	@FXML
	private ImageView horizontalVictory;
	
	@FXML
	private ImageView diagonalVictory;
	
}
