package morpions6;

import ai.QLearning;

// We set what happens at startup
public class Startup {

	public static void main(String[] args) {
		FXMainWindow.Create();
	}
	
	// The startup behaviour is called after the main window is created
	public static void StartupBehaviour()
	{
		GameManager.Initialize();
	}

}
