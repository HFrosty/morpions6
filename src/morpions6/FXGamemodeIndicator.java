package morpions6;

import java.io.File;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class FXGamemodeIndicator {
	
	static public void Update()
	{
		// We ask the ImageFactory to create an image according to the current gamemode (Singleplayer or Multiplayer)
		ImageView view = FXMainWindow.GetGamemodeIndicatorImageView();
		view.setImage(ImageFactory.CreateGamemodeImage(GameManager.GetGamemode()));
	}
	
}
