package morpions6;

//Used to determine of what type is a Symbol to check for victory and to give according Image
public class Cross implements Symbol {

	@Override
	public SymbolType Type() {
		return SymbolType.CROSS;
	}

}
