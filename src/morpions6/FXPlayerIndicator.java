package morpions6;

import java.io.File;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class FXPlayerIndicator {
	
	// Should fetch game manager to know which player is playing and use SymbolFactory to retrieve according Symbol
	static public void Update()
	{	
		PlayerID currentPlayer = GameManager.GetGrid().GetCurrentPlayer();
		Symbol symbol = SymbolFactory.CreateSymbolAccordingToPlayer(currentPlayer);
		ImageView view = FXMainWindow.GetPlayerIndicatorImageView();
		view.setImage(ImageFactory.BuildImageForSymbol(symbol));
	}
	
}
