package morpions6;

import java.util.ArrayList;

import javafx.animation.FadeTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

public class FXGrid {

	private Grid grid;
	private AnchorPane gridPane;
	private ImageView gridImageView;
	private Node[] symbols;
	
	public FXGrid(Grid grid)
	{
		gridPane  = FXMainWindow.GetGridPane();
		this.grid = grid;
		gridImageView = FXMainWindow.GetGridImageView();
		gridImageView.setImage(ImageFactory.CreateGridImage());
		symbols = new Node[9];
		SetupFX();
	}
	
	// Delete displayed symbols
	public void Reset()
	{
		// Delete each children except the first one which is the grid
		for (int i = gridPane.getChildren().size() - 1; i > 0; --i) {
			gridPane.getChildren().remove(i);
		}
	}
	
	// Receiving click data by pixel position (x, y) should convert it in position between (0..9) (9 excluded)
	public void OnClick(double clickPositionX, double clickPositionY)
	{
		int positionX = (int)clickPositionX / 133;
		int positionY = (int)clickPositionY / 133 * 3;
		grid.OnClick(positionX + positionY);
	}
	
	// Victory positions are given as the position of the symbols that trigered the victory
	public void LaunchVictoryAnimation(int victoryPosition0, int victoryPosition1, int victoryPosition2)
	{
		final int duration = 500;
		final double scale = 0.1;
		
		ScaleTransition transition = new ScaleTransition(Duration.millis(duration), symbols[victoryPosition0]);
		transition.setByX(scale);
		transition.setByY(scale);
		transition.setCycleCount(Timeline.INDEFINITE);
		transition.setAutoReverse(true);
		transition.play();
		
		transition = new ScaleTransition(Duration.millis(duration), symbols[victoryPosition1]);
		transition.setByX(scale);
		transition.setByY(scale);
		transition.setCycleCount(Timeline.INDEFINITE);
		transition.setAutoReverse(true);
		transition.play();
		
		transition = new ScaleTransition(Duration.millis(duration), symbols[victoryPosition2]);
		transition.setByX(scale);
		transition.setByY(scale);
		transition.setCycleCount(Timeline.INDEFINITE);
		transition.setAutoReverse(true);
		transition.play();
	}
	
	public void DrawSymbol(Symbol symbol, int position)
	{
		// Convert position between 0 and 9 to position in pixels
		double x = (position % 3) * (400.0 / 3.0) + 37;
		double y = (position / 3) * (400.0 / 3.0) - 62;
		
		ImageView symbolView = new ImageView();
		Image symbolImage = ImageFactory.BuildImageForSymbol(symbol);
		symbolView.setScaleX(100.0 / symbolImage.getWidth());
		symbolView.setScaleY(100.0 / symbolImage.getHeight());
		symbolView.setImage(symbolImage);
		
		FadeTransition transition = new FadeTransition(Duration.millis(500), symbolView);
		transition.setFromValue(0.0);
		transition.setToValue(1.0);
		transition.setCycleCount(1);
		transition.setAutoReverse(false);
		transition.play();
		
		gridPane.getChildren().add(symbolView);
		symbolView.setX(x);
		symbolView.setY(y);
		
		// We store the new symbol in an array to retrieve it when victory is declared
		symbols[position] = symbolView;
	}
	
	public void SetupFX()
	{
		gridImageView.setOnMouseClicked((event) -> 
		{
			OnClick(event.getX(), event.getY());
		});
	}
}
