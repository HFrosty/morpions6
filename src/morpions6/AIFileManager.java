package morpions6;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import ai.QLearning;

// Class used to retrieve, load and save files about AIs
public abstract class AIFileManager {
	
	public static final String FOLDER = "./aiconfig/";
	public static final String SUFFIX = ".ai";
	public static final String CURRENT = "current.cfg";
	
	// Used to retrieve all files about AI config
	public static File[] FetchAiFiles()
	{
		File directory = new File(FOLDER);
		File[] aiFiles = directory.listFiles(new FilenameFilter()
		{
			// We retrieve all files in the FOLDER that ends with the specified suffix
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(SUFFIX);
			}
		});
		return aiFiles;
	}
	
	// Used to retrieve an AI file of specified name
	public static File FetchAiFile(String name)
	{
		File file = new File(FOLDER + name);
		return file;
	}
	
	// Used to retrieve the AI that was last used by the user (when the program last exited)
	public static File FetchCurrentAi()
	{		
		File[] aiFiles = FetchAiFiles();
		
		// If there are no AI file, we create a new one with hardcoded parameters, save it, and set as current AI
		if(aiFiles == null || aiFiles.length == 0)
		{
			System.out.println("ERROR: Can't find AI files. Generating new one.");
			QLearning learning = new QLearning("Basic", 0.95, 0.9, 0.6, 2.0, 0.0, 1.0, 1000);
			AITraining thread = new AITraining(learning, false);
			thread.run();
			SaveAI(learning);
			SetCurrentAI(learning);
		}
		
		try
		{
			// We read the file named CURRENT as its content gives us the name of the last used AI file
			FileInputStream fInput = new FileInputStream(new File(FOLDER + CURRENT));
			ObjectInputStream oInput = new ObjectInputStream(fInput);
			String currentFileName = (String) oInput.readObject();
			oInput.close();
			return new File(FOLDER + currentFileName);
		}
		catch (ClassNotFoundException | IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	// Method used to save an AI from a QLearning object
	public static void SaveAI(QLearning learning)
	{
		try
		{
			FileOutputStream fOutput = new FileOutputStream(new File(FOLDER + learning.GetName() + SUFFIX));
			ObjectOutputStream oOutput = new ObjectOutputStream(fOutput);
			oOutput.writeObject(learning);
			oOutput.close();
			fOutput.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	// Method used to load an QLearning object from a File
	public static QLearning LoadAI(File file)
	{
		try
		{
			FileInputStream fileI = new FileInputStream(file);
			ObjectInputStream output = new ObjectInputStream(fileI);
			QLearning learning = (QLearning) output.readObject();
			output.close();
			fileI.close();
			return learning;
		}
		catch (IOException | ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	// We write in the CURRENT file the name of the AI
	public static void SetCurrentAI(QLearning learning)
	{
		File file = new File(FOLDER + CURRENT);
		if(file.exists())
		{
			file.delete();
		}
		
		try
		{
			FileOutputStream fOutput = new FileOutputStream(FOLDER + CURRENT);
			ObjectOutputStream oOutput = new ObjectOutputStream(fOutput);
			String aiFilename = learning.GetName() + SUFFIX;
			oOutput.writeObject(aiFilename);
			oOutput.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
}
